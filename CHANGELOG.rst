^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package casadi_vendor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

3.5.1 (2020-02-25)
------------------
* Updating README.
* Initial commit
* Contributors: Geoffrey Biggs, Joshua Whitley
